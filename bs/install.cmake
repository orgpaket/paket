# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------

if(BUILD_SOURCE)
  add_library(paket::libpaket ALIAS libpaket)
  install(TARGETS libpaket EXPORT paketTarget FILE_SET HEADERS)

  install(FILES ${PROJECT_BINARY_DIR}/lib/paketConfigVersion.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/paket/cmake/)

  install(FILES ${PROJECT_BINARY_DIR}/lib/paketConfig.cmake
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/paket/cmake)

  install(
    EXPORT paketTarget
    FILE paketTarget.cmake
    NAMESPACE paket::
    DESTINATION ${CMAKE_INSTALL_LIBDIR}/paket/cmake)
endif()


if(BUILD_DOC)
  install(DIRECTORY ${PROJECT_BINARY_DIR}/documentation
    DESTINATION ${CMAKE_INSTALL_DOCDIR})
endif()


