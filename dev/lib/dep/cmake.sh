# --------------------------------------------------------------------------------------------------
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: (C) 2022 Jayesh Badwaik <j.badwaik@fz-juelich.de>
# --------------------------------------------------------------------------------------------------
set -euo pipefail

# ATTENTION! Also update the corresponding version in CMakeLists.txt AND CMakePresets.json
CMAKE_VERSION=3.27.6
CMAKE_MIRROR="https://github.com/Kitware/CMake/releases/download/"
CMAKE_URL=$CMAKE_MIRROR/v$CMAKE_VERSION/cmake-$CMAKE_VERSION-linux-x86_64.sh
CMAKE_SHA256SUM="8c449dabb2b2563ec4e6d5e0fb0ae09e729680efab71527b59015131cea4a042"


SOURCE="${BASH_SOURCE[0]}"
# resolve $SOURCE until the file is no longer a symlink
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  # if $SOURCE was a relative symlink, we need to resolve it relative to the
  # path where the symlink file was located
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
SCRIPT_DIRECTORY="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"


source $SCRIPT_DIRECTORY/common.sh

process_cmdline_argument $@

TMP_DIR=$PKGROOT/var/tmp/cmake/

mkdir -p $TMP_DIR
mkdir -p $TMP_DIR/download

wget $CMAKE_URL -O $TMP_DIR/download/cmake.sh
pushd $TMP_DIR/download
if [[ $CMAKE_SHA256SUM != "SKIP" ]]; then
  echo "$CMAKE_SHA256SUM cmake.sh" | sha256sum --check
fi
bash cmake.sh --skip-license --prefix=$PKGROOT/usr
popd
