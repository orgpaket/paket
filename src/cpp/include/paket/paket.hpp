// -----------------------------------------------------------------------------
// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: (C) 2020, Jayesh Badwaik (jayeshbadwaik)
// -----------------------------------------------------------------------------

#ifndef PAKET_PAKET_HPP
#define PAKET_PAKET_HPP

#include <string>

namespace paket {
auto f(int a) -> int;
auto g(double b) -> double;
} // namespace paket

#endif // PAKET_PAKET_HPP
